export interface LoginFormProps {
  onGoogleLogIn: () => void;
  onFacebookLogIn: () => void;
  onTwitterLogIn: () => void;
}

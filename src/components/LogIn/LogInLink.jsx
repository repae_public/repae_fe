import React from 'react';
import { Link } from 'react-router-dom';
import ROUTES from '../../constants/routes';

const LoginLink = () => (
  <p>
    Don't have an account? <Link to={ROUTES.LOG_IN}>LogIn</Link>
  </p>
);
export default LoginLink;

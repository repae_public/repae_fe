import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Grid,
  Container,
  Box,
  Typography,
  Tabs,
  Tab,
  Card,
  CardContent,
  Button
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import {
  LoginWithTwitter,
  LoginWithGoogle,
  LoginWithFacebook
} from '../../redux/actions';

const StyledTabs = withStyles({
  indicator: {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    height: '6px',
    '& > div': {
      maxWidth: 40,
      height: '4px',
      borderRadius: '25px',
      width: '100%',
      backgroundColor: '#000'
    }
  }
})((props) => <Tabs {...props} TabIndicatorProps={{ children: <div /> }} />);

const StyledTab = withStyles((theme) => ({
  root: {
    textTransform: 'none',
    color: theme.palette.primary[900],
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(15),
    marginRight: theme.spacing(1),
    '&:focus': {
      opacity: 1
    }
  }
}))((props) => <Tab disableRipple {...props} />);

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      {...other}>
      {value === index && <Box p={0}>{children}</Box>}
    </Typography>
  );
}

export default (props) => {
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const dispatch = useDispatch();
  const onGoogleLogin = () => {
    dispatch(LoginWithGoogle());
  };
  const onFacebookLogIn = () => {
    dispatch(LoginWithFacebook());
  };
  const onTwitterLogIn = () => {
    dispatch(LoginWithTwitter());
  };

  return (
    <Grid item xs={12} md={8} lg={7} className="d-flex align-items-center">
      <Container maxWidth="sm">
        <div className="pt-5 pb-4">
          <StyledTabs
            value={value}
            indicatorColor="primary"
            textColor="primary"
            onChange={handleChange}>
            <StyledTab label="Iniciar sesión" />
            <StyledTab label="Crear cuenta" />
          </StyledTabs>
        </div>
        <TabPanel value={value} index={1}>
          <h3 className="display-4 mb-2 font-weight-bold">Registrate</h3>
          <Card className="mx-0 bg-secondary mt-0 w-100 p-0 mb-4 border-0">
            <CardContent className="p-3">
              <div className="card-header d-block p-3 mx-2 mb-0 mt-2 rounded border-0">
                <div className="text-muted text-center mb-3">
                  <span>Registrate con tus redes:</span>
                </div>
                <div className="text-center">
                  <Button
                    onClick={onGoogleLogin}
                    variant="outlined"
                    className="mr-2 text-google">
                    <span className="btn-wrapper--icon">
                      <FontAwesomeIcon icon={['fab', 'google']} />
                    </span>
                    <span className="btn-wrapper--label">Google</span>
                  </Button>
                  <Button
                    variant="outlined"
                    onClick={onFacebookLogIn}
                    className="mr-1 ml-1 text-facebook">
                    <span className="btn-wrapper--icon">
                      <FontAwesomeIcon icon={['fab', 'facebook']} />
                    </span>
                    <span className="btn-wrapper--label">Facebook</span>
                  </Button>
                  <Button
                    onClick={onTwitterLogIn}
                    variant="outlined"
                    className="ml-2 text-twitter">
                    <span className="btn-wrapper--icon">
                      <FontAwesomeIcon icon={['fab', 'twitter']} />
                    </span>
                    <span className="btn-wrapper--label">Twitter</span>
                  </Button>
                </div>
              </div>
            </CardContent>
          </Card>
        </TabPanel>
        <TabPanel value={value} index={0}>
          <h3 className="display-4 mb-2 font-weight-bold">Cuenta existente</h3>
          <Card className="mx-0 bg-secondary mt-0 w-100 p-0 mb-4 border-0">
            <CardContent className="p-3">
              <div className="card-header d-block p-3 mx-2 mb-0 mt-2 rounded border-0">
                <div className="text-muted text-center mb-3">
                  <span>Iniciar sesión con:</span>
                </div>
                <div className="text-center">
                  <Button
                    onClick={onGoogleLogin}
                    variant="outlined"
                    className="mr-2 text-google">
                    <span className="btn-wrapper--icon">
                      <FontAwesomeIcon icon={['fab', 'google']} />
                    </span>
                    <span className="btn-wrapper--label">Google</span>
                  </Button>
                  <Button
                    onClick={onFacebookLogIn}
                    variant="outlined"
                    className="mr-1 ml-1 text-facebook">
                    <span className="btn-wrapper--icon">
                      <FontAwesomeIcon icon={['fab', 'facebook']} />
                    </span>
                    <span className="btn-wrapper--label">Facebook</span>
                  </Button>
                  <Button
                    onClick={onTwitterLogIn}
                    variant="outlined"
                    className="ml-2 text-twitter">
                    <span className="btn-wrapper--icon">
                      <FontAwesomeIcon icon={['fab', 'twitter']} />
                    </span>
                    <span className="btn-wrapper--label">Twitter</span>
                  </Button>
                </div>
              </div>
            </CardContent>
          </Card>
        </TabPanel>
      </Container>
    </Grid>
  );
};

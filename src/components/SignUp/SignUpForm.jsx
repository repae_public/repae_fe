import React from 'react';

// Action Dispatchers

export default (props) => {
  const { onGoogleSignUp, onFacebookSignup, onTwitterSignup } = props;

  return (
    <form>
      <button type="button" onClick={onGoogleSignUp}>
        <img
          src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
          alt="logo"
        />
        Join With Google
      </button>
      <button type="button" onClick={onTwitterSignup}>
        <img
          src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
          alt="logo"
        />
        Join With Google
      </button>
      <button type="button" onClick={onFacebookSignup}>
        <img
          src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
          alt="logo"
        />
        Join With Google
      </button>
    </form>
  );
};

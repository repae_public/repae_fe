import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { TextField } from '@material-ui/core';

export default (props) => {
  const { value, handlers, institucion } = props;
  const institucionValue = value.data.find((x) => x.id === institucion);
  return (
    <Autocomplete
      disableClearable
      options={value.data}
      getOptionLabel={(option) => option.nombre}
      onChange={handlers.onChange}
      value={institucionValue}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Institución Responsable"
          variant="outlined"
          className="mb-3"
          fullWidth
          required
        />
      )}
    />
  );
};

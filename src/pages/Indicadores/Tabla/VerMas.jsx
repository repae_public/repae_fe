import React, { useState } from 'react';
import { Dialog, Tooltip, IconButton, Card, Divider } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ShortText from '../../Helpers/ShortText';
import ReferenciasList from '../../Helpers/ReferenciasList';
import ODSList from '../../Helpers/ODSList';

export default (props) => {
  const { indicador } = props;

  const [modal, setModal] = useState(false);
  const toggle = () => setModal((shown) => !shown);

  return (
    <div>
      <Tooltip arrow title="Ver más">
        <IconButton
          size="small"
          variant="outlined"
          color="secondary"
          onClick={toggle}>
          <FontAwesomeIcon icon={['fa', 'ellipsis-v']} />
        </IconButton>
      </Tooltip>
      <Dialog fullWidth open={modal} onClose={toggle}>
        <Card className="card-box p-4">
          <div>
            <div className="d-flex align-items-center mb-3">
              <div className="w-100">
                <span className="font-weight-bold font-size-lg">
                  {indicador.codigo} - {indicador.nombre}
                </span>
              </div>
            </div>
            <Divider />
            <div className="my-4 font-size-sm p-3 bg-secondary rounded-sm">
              <ShortText titulo="Descripción" valor={indicador.descripcion} />
              <ShortText titulo="Frecuencia" valor={indicador.frecuencia} />
              <ShortText titulo="Tendencia" valor={indicador.tendencias} />
              <ShortText titulo="Institución" valor={indicador.institucion} />
              <ShortText
                titulo="Unidad de Medida"
                valor={indicador.unidadMedida}
              />
              <ShortText titulo="Formula" valor={indicador.formula} />
              <ReferenciasList
                titulo="Referencias"
                valor={indicador.referencias}
              />
              <ODSList titulo="ODSs Vinculados" valor={indicador.ods} />
            </div>
          </div>
        </Card>
      </Dialog>
    </div>
  );
};

import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { TextField } from '@material-ui/core';

export default ({ value, handlers, tipoInstitucion }) => {
  const tipoValue = value.find((x) => x.id === tipoInstitucion);
  return (
    <Autocomplete
      disableClearable
      options={value}
      getOptionLabel={(option) => option.descripcion}
      onChange={handlers.onChange}
      value={tipoValue}
      renderOption={(option) => (
        <React.Fragment>
          {option.descripcion}{' '}
          <div className="ml-3">
            <strong>{option.siglas}</strong>
          </div>
        </React.Fragment>
      )}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Tipo"
          variant="outlined"
          className="mb-3"
          fullWidth
          required
        />
      )}
    />
  );
};

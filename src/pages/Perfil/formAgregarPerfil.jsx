import React, { useCallback } from 'react';
import { Button, Card, CardContent, Grid, TextField } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import AddIcon from '@material-ui/icons/Add';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

const rolSelect = [
  { title: 'Evaluador' },
  { title: 'Invitado' },
  { title: 'Administrador' }
];

const demo = [{ title: 'Demo' }, { title: 'Demo' }, { title: 'Demo' }];

export default () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const user = useSelector((state) => state.authentication.user);
  const userName = user
    .map((x) => x.additionalUserInfo.profile.name)
    .getDataOr('');
  const email = user
    .map((x) => x.additionalUserInfo.profile.email)
    .getDataOr('');

  const onCancelar = useCallback(() => {
    history.goBack();
  }, [history]);

  const onGuardar = useCallback(
    (event) => {
      event.preventDefault();
      //dispatch(AgregarIndicador(agregar.update(formulario)));
      history.goBack();
    },
    [history]
  );

  return (
    <Card className="card-box mb-4">
      <div className="card-header pr-2">
        <div className="card-header--title">
          <h6 className="font-size-lg mt-2 text-dark">Mi perfil</h6>
        </div>
      </div>
      <CardContent>
        <form>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={2}></Grid>

            <Grid item xs={12} sm={8}>
              <TextField
                fullWidth
                className="mb-3"
                label="Nombre"
                variant="outlined"
                value={userName}
              />
              <TextField
                fullWidth
                className="mb-3"
                label="Correo"
                variant="outlined"
                type="email"
                value={email}
              />
              <TextField
                fullWidth
                className="mb-3"
                label="Número de teléfono"
                variant="outlined"
                type="numeric"
              />
              <Autocomplete
                disabled
                options={rolSelect}
                getOptionLabel={(option) => option.title}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Rol"
                    variant="outlined"
                    fullWidth
                    disabled
                  />
                )}
              />
            </Grid>

            <Grid item xs={12} sm={2}></Grid>

            <Grid item xs={12} sm={2}></Grid>
            <Grid item xs={12} sm={7}>
              <Autocomplete
                options={demo}
                getOptionLabel={(option) => option.title}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Institución"
                    variant="outlined"
                    className="mb-3"
                    fullWidth
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={1}>
              <Button
                className="mt-2 ml-2"
                variant="contained"
                color="secondary"
                startIcon={<AddIcon />}>
                Agregar
              </Button>
            </Grid>
            <Grid item xs={12} sm={2}></Grid>
          </Grid>
        </form>
      </CardContent>
      <div className="card-footer p-3 text-right">
        <Button
          className="mr-3"
          size="small"
          color="primary"
          onClick={onCancelar}
          variant="contained">
          <span className="btn-wrapper--label">Cancelar</span>
        </Button>
        <Button
          size="small"
          color="secondary"
          type="submit"
          onClick={onGuardar}
          variant="contained">
          <span className="btn-wrapper--label">Guardar</span>
        </Button>
      </div>
    </Card>
  );
};

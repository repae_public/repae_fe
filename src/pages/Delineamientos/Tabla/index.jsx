import React, { useEffect } from 'react';
import AddIcon from '@material-ui/icons/Add';
import { Delineamientos } from '../../../redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import DelineamientosTable from './delineamientosTable';
import ResourceRender from 'ghost-stories/dist/react/resource';
import { Card, CardContent, Button } from '@material-ui/core';

import { Link } from 'react-router-dom';
import { OnQuery, OnError, OnEmpty } from '../../ResourceHelpers';
import ROUTES from '../../../constants/routes';

export default () => {
  const dispatch = useDispatch();
  const delineamientos = useSelector((state) => state.delineamientos.lista);
  useEffect(() => {
    dispatch(Delineamientos(delineamientos.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Card className="card-box mb-4">
      <div className="card-header pr-2">
        <div className="card-header--title">
          <h6 className="font-size-lg mt-2 text-dark">Lineamientos</h6>
        </div>
        <div className="card-header--actions">
          <Button
            size="small"
            variant="contained"
            color="secondary"
            component={Link}
            to={ROUTES.DELINEAMIENTOS_AGREGAR}
            startIcon={<AddIcon />}>
            Agregar
          </Button>
        </div>
      </div>
      <CardContent className="p-3">
        <ResourceRender
          resource={delineamientos}
          Data={DelineamientosTable}
          Query={OnQuery}
          Empty={OnError}
          Error={OnEmpty}
        />
      </CardContent>
    </Card>
  );
};

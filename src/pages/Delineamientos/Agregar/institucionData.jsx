import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { TextField } from '@material-ui/core';

export default ({ value, handlers, institucion }) => {
  const tipoValue = value.data.find((x) => x.id === institucion);
  return (
    <Autocomplete
      disableClearable
      options={value.data}
      getOptionLabel={(option) => option.nombre}
      onChange={handlers.onChange}
      value={tipoValue}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Institución"
          variant="outlined"
          fullWidth
          required
        />
      )}
    />
  );
};

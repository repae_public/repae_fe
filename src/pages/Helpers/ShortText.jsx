import React from 'react';
export default ({ titulo, valor }) => (
  <div className="d-flex justify-content-between py-2">
    <span className="font-weight-bold">{titulo}</span>
    <span className="text-black-50">{valor}</span>
  </div>
);

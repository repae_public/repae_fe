import React from 'react';

export default ({ titulo, valor }) => {
  return (
    <>
      <div className="d-flex justify-content-between ">
        <span className="font-weight-bold">{titulo}</span>
      </div>
      <div className="d-flex justify-content-between ">
        <ul>
          {valor.map((referencia) => (
            <li key={Math.random().toString(10)}>
              <a href={referencia.url}>
                <span className="font-weight-bold">{referencia.titulo}: </span>
                <span>{referencia.url}</span>
              </a>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

import React, { useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { FIX_URL } from 'ghost-stories/dist/constants';
import { useParams, useHistory } from 'react-router-dom';
import {
  EditarUsuario,
  GestionUsuarios,
  LimpiarUsuario,
  ModificarUsuario
} from '../../../redux/actions';
import FormularioUsuarios from '../FormUsuarios';

export default () => {
  const { id } = useParams();

  const history = useHistory();
  const dispatch = useDispatch();
  const formulario = useSelector((state) => state.gestionUsuarios.formulario);
  const gestionUsuarios = useSelector((state) => state.gestionUsuarios.lista);

  const original = useSelector((state) =>
    state.gestionUsuarios.lista
      .map((xs) => xs.data.find((x) => x.id === id))
      .getDataOr(null)
  );

  useEffect(() => {
    dispatch(GestionUsuarios(gestionUsuarios.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (original === null) {
      return history.goBack();
    }
    dispatch(EditarUsuario(original));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onChangeNombre = useCallback(
    (event) => {
      dispatch(EditarUsuario({ ...formulario, nombre: event.target.value }));
    },
    [formulario, dispatch]
  );

  const onChangeCorreo = useCallback(
    (event) => {
      dispatch(EditarUsuario({ ...formulario, correo: event.target.value }));
    },
    [formulario, dispatch]
  );

  const onCancelar = useCallback(() => {
    dispatch(LimpiarUsuario({}));
  }, [dispatch]);

  const onGuardar = useCallback(
    (event) => {
      event.preventDefault();
      dispatch(
        ModificarUsuario(Resource.Query({ [FIX_URL]: id, ...formulario }))
      );
      history.goBack();
    },
    [dispatch, history, id, formulario]
  );
  const onChangeRol = useCallback(
    (event, rol) => {
      dispatch(EditarUsuario({ ...formulario, rol: rol.title }));
    },
    [formulario, dispatch]
  );

  return (
    <FormularioUsuarios
      {...{
        title: 'Editar Usuario',
        onGuardar,
        formulario,
        onCancelar,
        onChangeNombre,
        onChangeRol,
        onChangeCorreo
      }}
    />
  );
};

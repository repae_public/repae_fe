import React, { useState } from 'react';
import { Dialog, Tooltip, IconButton, Card, Divider } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import avatar4 from '../../../assets/images/avatars/avatar4.jpg';

export default (props) => {
  const { usuario } = props;

  const [modal1, setModal1] = useState(false);
  const toggle1 = () => setModal1(!modal1);

  return (
    <div>
      <Tooltip arrow title="Ver más">
        <IconButton
          size="small"
          variant="outlined"
          color="secondary"
          onClick={toggle1}>
          <FontAwesomeIcon icon={['fa', 'ellipsis-v']} />
        </IconButton>
      </Tooltip>
      <Dialog fullWidth open={modal1} onClose={toggle1}>
        <Card className="card-box p-4">
          <div>
            <div className="d-flex align-items-center mb-3">
              <div className="avatar-icon-wrapper rounded-circle mr-3">
                <div className="d-block p-0 avatar-icon-wrapper m-0 d-100">
                  <div className="rounded-circle overflow-hidden">
                    <img alt="..." className="img-fluid" src={avatar4} />
                  </div>
                </div>
              </div>
              <div className="w-100">
                <a
                  href="#/"
                  onClick={(e) => e.preventDefault()}
                  className="font-weight-bold font-size-lg"
                  title="...">
                  {usuario.nombre}
                </a>
                <span className="text-black-50 d-block">
                  {usuario.apellido}
                </span>
              </div>
            </div>
            <Divider />
            <div className="my-4 font-size-sm p-3 bg-secondary rounded-sm">
              <div className="d-flex justify-content-between">
                <span className="font-weight-bold">Correo:</span>
                <span className="text-black-50">{usuario.correo}</span>
              </div>
              <div className="d-flex justify-content-between py-2">
                <span className="font-weight-bold">Institución:</span>
                <span className="text-black-50">{usuario.institucion}</span>
              </div>
              <div className="d-flex justify-content-between">
                <span className="font-weight-bold">Rol:</span>
                <span className="text-black-50">{usuario.rol}</span>
              </div>
              <div className="d-flex justify-content-between pt-2">
                <span className="font-weight-bold">Número de teléfono:</span>
                <span className="text-black-50">{usuario.numero_telefono}</span>
              </div>
            </div>
          </div>
        </Card>
      </Dialog>
    </div>
  );
};

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import usuariosTable from './usuariosTable';
import ResourceRender from 'ghost-stories/dist/react/resource';
import { GestionUsuarios } from '../../../redux/actions';
import { Card, CardContent, Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';
import { OnQuery, OnError, OnEmpty } from '../../ResourceHelpers';
import ROUTES from '../../../constants/routes';
export default () => {
  const dispatch = useDispatch();
  const gestionUsuarios = useSelector((state) => state.gestionUsuarios.lista);
  useEffect(() => {
    dispatch(GestionUsuarios(gestionUsuarios.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Card className="card-box mb-4">
      <div className="card-header pr-2">
        <div className="card-header--title">
          <h6 className="font-size-lg mt-2 text-dark">Usuarios</h6>
        </div>
        <div className="card-header--actions">
          <Button
            size="small"
            variant="contained"
            color="secondary"
            component={Link}
            to={ROUTES.USUARIOS_AGREGAR}
            startIcon={<AddIcon />}>
            Agregar
          </Button>
        </div>
      </div>
      <CardContent className="p-3">
        <ResourceRender
          resource={gestionUsuarios}
          Data={usuariosTable}
          Query={OnQuery}
          Empty={OnError}
          Error={OnEmpty}
        />
      </CardContent>
    </Card>
  );
};

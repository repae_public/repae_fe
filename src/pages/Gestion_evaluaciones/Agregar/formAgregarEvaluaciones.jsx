import React, { useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Resource } from 'ghost-stories/dist/unions/Resource';

import { useHistory } from 'react-router-dom';

import {
  Indicadores,
  Delineamientos,
  EditarEvaluacion,
  Sexo,
  RangoEtario,
  Region,
  LimpiarEvaluacion,
  AgregarEvaluacion
} from '../../../redux/actions';

import FormularioEvaluaciones from '../FormEvaluaciones';

export default () => {
  const dispatch = useDispatch();
  const formulario = useSelector((state) => state.evaluaciones.formulario);
  const history = useHistory();
  const delineamientos = useSelector((state) => state.delineamientos.lista);
  const indicadores = useSelector((state) => state.indicadores.lista);
  const sexo = useSelector((state) => state.parametros.sexo);
  const rango_etario = useSelector((state) => state.parametros.rango_etario);
  const region = useSelector((state) => state.parametros.region);
  const agregar = useSelector((state) => state.evaluaciones.agregar);

  const parametros = Resource.concat([
    indicadores,
    sexo,
    rango_etario,
    region
  ]).map(([indicadores, sexo, rango_etario, region]) => ({
    indicadores,
    sexo,
    rango_etario,
    region
  }));

  useEffect(() => {
    dispatch(Delineamientos(delineamientos.update()));
    dispatch(Indicadores(indicadores.update()));
    dispatch(Sexo(sexo.update()));
    dispatch(RangoEtario(rango_etario.update()));
    dispatch(Region(region.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onChangeField = useCallback(
    (event) => {
      dispatch(
        EditarEvaluacion({
          ...formulario,
          [event.target.name]: event.target.value
        })
      );
    },
    [formulario, dispatch]
  );

  const onChangeDeineamiento = useCallback(
    (event, lineamiento) => {
      dispatch(
        EditarEvaluacion({ ...formulario, lineamiento: lineamiento.id })
      );
    },
    [formulario, dispatch]
  );

  const onChangeEstado = useCallback(
    (event, estado) => {
      dispatch(EditarEvaluacion({ ...formulario, estado: estado.title }));
    },
    [formulario, dispatch]
  );
  const onChangeResumen = useCallback(
    (event) => {
      dispatch(EditarEvaluacion({ ...formulario, resumen: event }));
    },
    [formulario, dispatch]
  );

  const onCancelar = () => {
    dispatch(LimpiarEvaluacion({}));
    history.goBack();
  };

  const onGuardar = useCallback(
    (event) => {
      event.preventDefault();
      dispatch(AgregarEvaluacion(agregar.update(formulario)));
      history.goBack();
    },
    [dispatch, agregar, history, formulario]
  );

  const onChangeIndicadores = useCallback(
    (datos) => {
      dispatch(EditarEvaluacion({ ...formulario, indicadores: datos }));
    },
    [dispatch, formulario]
  );

  return (
    <FormularioEvaluaciones
      {...{
        title: 'Agregar Evaluacion',
        onGuardar,
        delineamientos,
        formulario,
        parametros,
        onCancelar,
        onChangeIndicadores,
        onChangeEstado,
        onChangeResumen,
        onChangeDeineamiento,
        onChangeField
      }}
    />
  );
};

import React from 'react';
import { merge } from 'ramda';
import MaterialTable from 'material-table';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { TextField } from '@material-ui/core';

const makeLista = (xs, campo) =>
  xs.map((i) => ({ [i.id]: i[campo || 'nombre'] })).reduce(merge, {});

export default ({ value, handlers, initialData }) => {
  const { indicadores, sexo, rango_etario, region } = value;
  const sexoLista = makeLista(sexo);
  const regionLista = makeLista(region);
  const indicadoresLista = makeLista(indicadores);
  const rangoEtarioLista = makeLista(rango_etario, 'rango');
  const [data, setData] = React.useState(initialData || []);
  return (
    <MaterialTable
      className="mb-3"
      title="Agregar Indicadores"
      columns={[
        {
          title: 'Indicador',
          field: 'indicador',
          lookup: indicadoresLista,
          editComponent: (props) => (
            <Autocomplete
              disableClearable
              options={indicadores}
              onChange={(event, value) => {
                props.onChange(value.id);
              }}
              getOptionLabel={(option) => option.nombre}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Indicador"
                  className="mb-3"
                  required
                  fullWidth
                />
              )}
            />
          )
        },
        {
          title: 'Sexo',
          field: 'sexo',
          lookup: sexoLista,
          editComponent: (props) => (
            <Autocomplete
              disableClearable
              options={sexo}
              onChange={(event, value) => {
                props.onChange(value.id);
              }}
              getOptionLabel={(option) => option.nombre}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Sexo"
                  className="mb-3"
                  required
                  fullWidth
                />
              )}
            />
          )
        },
        {
          title: 'Edad',
          field: 'edad',
          lookup: rangoEtarioLista,
          editComponent: (props) => (
            <Autocomplete
              disableClearable
              options={rango_etario}
              onChange={(event, value) => {
                props.onChange(value.id);
              }}
              getOptionLabel={(option) => option.rango}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Edad"
                  className="mb-3"
                  required
                  fullWidth
                />
              )}
            />
          )
        },
        {
          title: 'Geografia',
          field: 'geografia',
          lookup: regionLista,
          editComponent: (props) => (
            <Autocomplete
              disableClearable
              options={region}
              onChange={(event, value) => {
                props.onChange(value.id);
              }}
              getOptionLabel={(option) => option.nombre}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Geografia"
                  className="mb-3"
                  required
                  fullWidth
                />
              )}
            />
          )
        },
        {
          title: 'Otras dimensiones',
          field: 'otrasDimensiones'
        },
        { title: 'Valor de referencia', field: 'v_referencia' },
        { title: 'Valor medido', field: 'v_medido' }
      ]}
      data={data}
      editable={{
        onRowAdd: (newData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              const current = data.concat([newData]);
              setData(current);
              handlers.onChange(current);
              resolve();
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              if (oldData) {
                const current = data.map((x, i) =>
                  i === data.indexOf(oldData) ? newData : x
                );
                setData(current);
                handlers.onChange(current);
              }
              resolve();
            }, 600);
          }),
        onRowDelete: (oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              const current = data.filter(
                (x, i) => i !== data.indexOf(oldData)
              );
              handlers.onChange(current);
              setData(current);
              resolve();
            }, 600);
          })
      }}
      options={{
        actionsColumnIndex: -1
      }}
      localization={{
        pagination: {
          labelDisplayedRows: '{from}-{to} de {count}',
          labelRowsSelect: 'filas',
          firstTooltip: 'Primera página',
          lastTooltip: 'Ultima página',
          previousTooltip: 'Anterior',
          nextTooltip: 'Siguiente'
        },
        toolbar: {
          nRowsSelected: '{0} file(s) seleccionada',
          searchPlaceholder: 'Buscar',
          searchTooltip: 'Buscar'
        },
        header: {
          actions: 'Acciones'
        },
        body: {
          emptyDataSourceMessage: 'No se cargaron datos',
          addTooltip: 'Agregar',
          editTooltip: 'Editar',
          deleteTooltip: 'Eliminar',
          editRow: {
            saveTooltip: 'Guardar',
            cancelTooltip: 'Cancelar',
            deleteText: '¿Está seguro que desea eliminar esta fila?'
          }
        }
      }}
    />
  );
};

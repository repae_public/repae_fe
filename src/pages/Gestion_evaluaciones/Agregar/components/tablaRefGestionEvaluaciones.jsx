import React from 'react';
import { Card, Grid, CardContent } from '@material-ui/core';
import FormAgregarRefEvaluaciones from './formAgregarRefGestionEvaluaciones';
import ReferenciaCard from './referenciaCard';
import { useSelector } from 'react-redux';

export default () => {
  const referencias = useSelector(
    (state) => state.evaluaciones.formulario.referencias
  );
  return (
    <div className="mb-3">
      <Grid item xs={12}>
        <Card className="card-box mb-4">
          <div className="card-header">
            <div className="card-header--title font-size-md font-weight-bold py-2">
              Referencias
            </div>
          </div>
          <CardContent>
            {referencias.map((referencia, i) => (
              <ReferenciaCard
                key={i.toString()}
                referencia={referencia}
                indice={i}
              />
            ))}
          </CardContent>
          <FormAgregarRefEvaluaciones />
        </Card>
      </Grid>
    </div>
  );
};

import React, { useCallback } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import { IconButton, Tooltip } from '@material-ui/core';
import { FIX_URL } from 'ghost-stories/dist/constants';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { EliminarEvaluacion } from '../../../redux/actions';
import { useDispatch } from 'react-redux';
import VerMas from './VerMas';
import ROUTES from '../../../constants/routes';

export default (props) => {
  const { evaluacion } = props;
  const dispatch = useDispatch();
  const onDeleteEvaluacion = useCallback(() => {
    dispatch(EliminarEvaluacion(Resource.Query({ [FIX_URL]: evaluacion.id })));
  }, [dispatch, evaluacion]);

  return (
    <tr key={evaluacion.id}>
      <td className="text-left">{evaluacion.titulo}</td>
      <td className="text-left">
        <div className="badge badge-info px-4">{evaluacion.estado}</div>
      </td>
      <td className="text-left">{evaluacion.marco}</td>

      <td>
        <Tooltip arrow title="Editar">
          <IconButton
            size="small"
            variant="outlined"
            color="secondary"
            component={Link}
            to={`${ROUTES.EVALUACIONES_EDITAR.replace(':id', evaluacion.id)}`}>
            <FontAwesomeIcon icon={['fa', 'edit']} />
          </IconButton>
        </Tooltip>
      </td>

      <td className="text-center">
        <Tooltip arrow title="Eliminar">
          <IconButton
            onClick={onDeleteEvaluacion}
            size="small"
            variant="outlined"
            color="secondary">
            <FontAwesomeIcon icon={['fa', 'trash']} />
          </IconButton>
        </Tooltip>
      </td>

      <td className="text-center">
        <VerMas evaluacion={evaluacion} />
      </td>
    </tr>
  );
};

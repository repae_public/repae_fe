import React, { useState } from 'react';
import { Dialog, Tooltip, IconButton, Card, Divider } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ShortText from '../../Helpers/ShortText';
import DratfJSEntry from '../../Helpers/DratfJSEntry';
import ReferenciasList from '../../Helpers/ReferenciasList';
import IndicadoresList from '../../Helpers/IndicadoresList';
import { useSelector } from 'react-redux';
import { Resource } from 'ghost-stories/dist/unions/Resource';
export default (props) => {
  const { evaluacion } = props;

  const [modal, setModal] = useState(false);
  const toggle = () => setModal((shown) => !shown);
  const indicadores = useSelector((state) => state.indicadores.lista);

  const lineamiento = useSelector((state) =>
    state.delineamientos.lista
      .map((x) => x.data.find((x) => x.id === evaluacion.lineamiento))
      .map((x) => (x ? x.nombre : ''))
      .getDataOr('')
  );
  const { sexo, region, rango_etario } = useSelector(
    (state) => state.parametros
  );
  const parametros = Resource.concat([indicadores, sexo, rango_etario, region])
    .map(([indicadores, sexo, rango_etario, region]) => ({
      indicadores,
      sexo,
      rango_etario,
      region
    }))
    .getDataOr({
      indicadores: [],
      sexo: [],
      rango_etario: [],
      region: []
    });

  return (
    <div>
      <Tooltip arrow title="Ver más">
        <IconButton
          size="small"
          variant="outlined"
          color="secondary"
          onClick={toggle}>
          <FontAwesomeIcon icon={['fa', 'ellipsis-v']} />
        </IconButton>
      </Tooltip>
      <Dialog
        fullWidth
        maxWidth="lg"
        open={modal}
        onClose={toggle}
        scroll="body">
        <Card className="card-box p-4">
          <div>
            <div className="d-flex align-items-center mb-3">
              <div className="w-100">
                <span className="font-weight-bold font-size-lg">
                  {evaluacion.titulo}
                </span>
              </div>
            </div>
            <Divider />
            <div className="my-4 font-size-sm p-3 bg-secondary rounded-sm">
              <ShortText
                titulo="Marco de la Evaluacion (Proyecto)"
                valor={evaluacion.marco}
              />
              <ShortText titulo="Estado" valor={evaluacion.estado} />
              <ShortText titulo="Lineamiento" valor={lineamiento} />
              <DratfJSEntry titulo="Resumen" valor={evaluacion.resumen} />
              <IndicadoresList
                titulo="Indicadores"
                valor={evaluacion.indicadores || []}
                parametros={parametros}
              />
              <ReferenciasList
                titulo="Referencias"
                valor={evaluacion.referencias}
              />
            </div>
          </div>
        </Card>
      </Dialog>
    </div>
  );
};

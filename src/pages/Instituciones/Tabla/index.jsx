import React, { useEffect } from 'react';
import { Instituciones, TipoInstitucion } from '../../../redux/actions';
import { propOr } from 'ramda';
import { useDispatch, useSelector } from 'react-redux';
import InstitucionesTable from './InstitucionesTable';
import ResourceRender from 'ghost-stories/dist/react/resource';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import AddIcon from '@material-ui/icons/Add';
import { Card, CardContent, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { OnQuery, OnError, OnEmpty } from '../../ResourceHelpers';
import ROUTES from '../../../constants/routes';

export default () => {
  const dispatch = useDispatch();

  const instituciones = useSelector((state) => state.instituciones.lista);
  const tipo_institucion = useSelector(
    (state) => state.parametros.tipo_institucion
  );

  useEffect(() => {
    dispatch(Instituciones(instituciones.update()));
    dispatch(TipoInstitucion(tipo_institucion.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const institucionesFull = Resource.concat([
    instituciones,
    tipo_institucion
  ]).map(([instituciones, tipo_institucion]) =>
    instituciones.data.map((institucion) =>
      Object.assign({}, institucion, {
        tipo: propOr(
          '',
          'descripcion',
          tipo_institucion.find((tipo) => tipo.id === institucion.tipo)
        )
      })
    )
  );

  return (
    <Card className="card-box mb-4">
      <div className="card-header pr-2">
        <div className="card-header--title">
          <h6 className="font-size-lg mt-2 text-dark">Instituciones</h6>
        </div>
        <div className="card-header--actions">
          <Button
            size="small"
            variant="contained"
            color="secondary"
            component={Link}
            to={ROUTES.INSTITUCIONES_AGREGAR}
            startIcon={<AddIcon />}>
            Agregar
          </Button>
        </div>
      </div>
      <CardContent className="p-3">
        <ResourceRender
          resource={institucionesFull}
          Data={InstitucionesTable}
          Query={OnQuery}
          Empty={OnEmpty}
          Error={OnError}
        />
      </CardContent>
    </Card>
  );
};

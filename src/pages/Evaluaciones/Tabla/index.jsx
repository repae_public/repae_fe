import React, { useEffect } from 'react';
import {
  Evaluaciones,
  Delineamientos,
  Indicadores,
  Sexo,
  RangoEtario,
  Region
} from '../../../redux/actions';
import AddIcon from '@material-ui/icons/Add';
import ResourceRender from 'ghost-stories/dist/react/resource';
import EvaluacionesTable from './EvaluacionesTable';
import { useDispatch, useSelector } from 'react-redux';
import { Card, CardContent, Button } from '@material-ui/core';

import { Link } from 'react-router-dom';
import { OnQuery, OnError, OnEmpty } from '../../ResourceHelpers';
import ROUTES from '../../../constants/routes';

export default () => {
  const dispatch = useDispatch();
  const evaluaciones = useSelector((state) => state.evaluaciones.lista);
  const delineamientos = useSelector((state) => state.delineamientos.lista);
  const indicadores = useSelector((state) => state.indicadores.lista);
  const sexo = useSelector((state) => state.parametros.sexo);
  const rango_etario = useSelector((state) => state.parametros.rango_etario);
  const region = useSelector((state) => state.parametros.region);

  useEffect(() => {
    dispatch(Evaluaciones(evaluaciones.update()));
    dispatch(Delineamientos(delineamientos.update()));
    dispatch(Delineamientos(delineamientos.update()));
    dispatch(Indicadores(indicadores.update()));
    dispatch(Sexo(sexo.update()));
    dispatch(RangoEtario(rango_etario.update()));
    dispatch(Region(region.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Card className="card-box mb-4">
      <div className="card-header pr-2">
        <div className="card-header--title">
          <h6 className="font-size-lg mt-2 text-dark">Evaluaciones</h6>
        </div>
        <div className="card-header--actions">
          <Button
            size="small"
            variant="contained"
            color="secondary"
            component={Link}
            to={ROUTES.EVALUACIONES_AGREGAR}
            startIcon={<AddIcon />}>
            Agregar
          </Button>
        </div>
      </div>
      <CardContent className="p-3">
        <ResourceRender
          resource={evaluaciones}
          Data={EvaluacionesTable}
          Query={OnQuery}
          Empty={OnError}
          Error={OnEmpty}
        />
      </CardContent>
    </Card>
  );
};

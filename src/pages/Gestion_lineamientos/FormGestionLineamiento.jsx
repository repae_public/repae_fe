import React from 'react';
import { Button, Card, CardContent, Grid, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import ResourceRender from 'ghost-stories/dist/react/resource';
import TablaOdsDelineamientos from './Agregar/components/tablaOdsDelineamientos';
import institucionData from './Agregar/institucionData';
import TipoLineamientoData from './Agregar/TipoLineamientoData';
import { OnQuery, OnError, OnEmpty } from '../ResourceHelpers';

export default (props) => {
  const {
    formulario,
    onChangeTipoLineamiento,
    onChangeInstitucion,
    onCancelar,
    onAgregarInsitucion,
    onGuardar,
    tipo_lineamiento,
    instituciones,
    title,
    onChangeField
  } = props;
  return (
    <form onSubmit={onGuardar}>
      <Card className="card-box mb-4">
        <div className="card-header pr-2">
          <div className="card-header--title">
            <h6 className="font-size-lg mt-2 text-dark">{title}</h6>
          </div>
        </div>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
              <TextField
                fullWidth
                className="mb-3"
                label="Código"
                variant="outlined"
                name="codigo"
                value={formulario.codigo}
                onChange={onChangeField}
                required
              />
              <TextField
                fullWidth
                className="mb-3"
                label="Nombre"
                variant="outlined"
                name="nombre"
                value={formulario.nombre}
                onChange={onChangeField}
                required
              />
              <ResourceRender
                resource={tipo_lineamiento}
                Data={TipoLineamientoData}
                Query={OnQuery}
                Empty={OnError}
                Error={OnEmpty}
                props={{ tipo_lineamiento: formulario.tipo }}
                handlers={{ onChange: onChangeTipoLineamiento }}
              />
            </Grid>
            <Grid item xs={12} sm={10}>
              <ResourceRender
                resource={instituciones}
                Data={institucionData}
                Query={OnQuery}
                Empty={OnError}
                Error={OnEmpty}
                props={{ institucion: formulario.institucion }}
                handlers={{ onChange: onChangeInstitucion }}
              />
            </Grid>
            <Grid item xs={12} sm={2}>
              <Button
                className="mt-2 ml-2"
                variant="contained"
                color="secondary"
                onClick={onAgregarInsitucion}
                startIcon={<AddIcon />}>
                Agregar
              </Button>
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                className="mb-3"
                label="Descripción"
                multiline
                rows="5"
                variant="outlined"
                name="descripcion"
                value={formulario.descripcion}
                onChange={onChangeField}
                fullWidth
                required
              />
              <TablaOdsDelineamientos />
            </Grid>
          </Grid>
        </CardContent>
        <div className="card-footer p-3 text-right">
          <Button
            className="mr-3"
            size="small"
            color="primary"
            onClick={onCancelar}
            variant="contained">
            <span className="btn-wrapper--label">Cancelar</span>
          </Button>
          <Button
            size="small"
            color="secondary"
            type="submit"
            variant="contained">
            <span className="btn-wrapper--label">Guardar</span>
          </Button>
        </div>
      </Card>
    </form>
  );
};

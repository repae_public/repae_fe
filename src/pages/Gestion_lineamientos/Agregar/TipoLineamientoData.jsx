import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { TextField } from '@material-ui/core';

export default ({ value, handlers, tipo_lineamiento }) => {
  const tipoValue = value.find((x) => x.id === tipo_lineamiento);
  return (
    <Autocomplete
      disableClearable
      options={value}
      getOptionLabel={(option) => option.descripcion}
      onChange={handlers.onChange}
      value={tipoValue}
      renderOption={(option) => (
        <React.Fragment>
          {option.descripcion}
          <strong className="ml-3">{option.siglas}</strong>
        </React.Fragment>
      )}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Tipo"
          variant="outlined"
          className="mb-3"
          fullWidth
          required
        />
      )}
    />
  );
};

import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import {
  AgregarLineamiento,
  EditarLineamiento,
  Instituciones,
  LimpiarLineamiento,
  TipoLineamiento
} from '../../../redux/actions';
import ROUTES from '../../../constants/routes';
import FormularioLineamiento from '../FormGestionLineamiento';

export default () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const formulario = useSelector((state) => state.delineamientos.formulario);
  const instituciones = useSelector((state) => state.instituciones.lista);
  const user = useSelector((state) => state.authentication.user);
  const userId = user.map((x) => x.additionalUserInfo.profile.id).getDataOr('');
  const userName = user
    .map((x) => x.additionalUserInfo.profile.name)
    .getDataOr('');

  useEffect(() => {
    dispatch(
      EditarLineamiento({
        ...formulario,
        creadorNombre: userName,
        creadorId: userId
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const tipo_lineamiento = useSelector(
    (state) => state.parametros.tipo_lineamiento
  );
  const agregar = useSelector((state) => state.delineamientos.agregar);

  useEffect(() => {
    dispatch(TipoLineamiento(tipo_lineamiento.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    dispatch(Instituciones(instituciones.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onChangeField = useCallback(
    (event) => {
      dispatch(
        EditarLineamiento({
          ...formulario,
          [event.target.name]: event.target.value
        })
      );
    },
    [formulario, dispatch]
  );

  const onChangeTipoLineamiento = useCallback(
    (event, lineamiento) => {
      dispatch(EditarLineamiento({ ...formulario, tipo: lineamiento.id }));
    },
    [formulario, dispatch]
  );

  const onChangeInstitucion = useCallback(
    (event, institucion) => {
      dispatch(
        EditarLineamiento({ ...formulario, institucion: institucion.id })
      );
    },
    [formulario, dispatch]
  );

  const onCancelar = () => {
    dispatch(LimpiarLineamiento({}));
    history.goBack();
  };

  const onAgregarInsitucion = useCallback(() => {
    history.push(ROUTES.INSTITUCIONES_AGREGAR);
  }, [history]);

  const onGuardar = useCallback(
    (event) => {
      event.preventDefault();
      dispatch(AgregarLineamiento(agregar.update(formulario)));
      history.goBack();
    },
    [dispatch, agregar, formulario, history]
  );
  return (
    <FormularioLineamiento
      {...{
        title: 'Agregar Lineamiento',
        tipo_lineamiento,
        formulario,
        onChangeTipoLineamiento,
        onChangeInstitucion,
        onCancelar,
        onAgregarInsitucion,
        onGuardar,
        instituciones,
        onChangeField
      }}
    />
  );
};

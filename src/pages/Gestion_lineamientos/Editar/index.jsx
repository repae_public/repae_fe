import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { FIX_URL } from 'ghost-stories/dist/constants';
import {
  EditarLineamiento,
  Instituciones,
  LimpiarLineamiento,
  ModificarLineamiento,
  TipoLineamiento
} from '../../../redux/actions';
import ROUTES from '../../../constants/routes';
import FormularioLineamiento from '../FormGestionLineamiento';

export default () => {
  const { id } = useParams();

  const history = useHistory();
  const dispatch = useDispatch();
  const formulario = useSelector((state) => state.delineamientos.formulario);
  const instituciones = useSelector((state) => state.instituciones.lista);
  const tipo_lineamiento = useSelector(
    (state) => state.parametros.tipo_lineamiento
  );

  const original = useSelector((state) =>
    state.delineamientos.lista
      .map((xs) => xs.data.find((x) => x.id === id))
      .getDataOr(null)
  );

  useEffect(() => {
    dispatch(TipoLineamiento(tipo_lineamiento.update()));
    dispatch(Instituciones(instituciones.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (original === null) {
      return history.goBack();
    }
    dispatch(EditarLineamiento(original));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [original]);

  const onChangeField = useCallback(
    (event) => {
      dispatch(
        EditarLineamiento({
          ...formulario,
          [event.target.name]: event.target.value
        })
      );
    },
    [formulario, dispatch]
  );

  const onChangeTipoLineamiento = useCallback(
    (event, lineamiento) => {
      dispatch(EditarLineamiento({ ...formulario, tipo: lineamiento.id }));
    },
    [formulario, dispatch]
  );

  const onChangeInstitucion = useCallback(
    (event, institucion) => {
      dispatch(
        EditarLineamiento({ ...formulario, institucion: institucion.id })
      );
    },
    [formulario, dispatch]
  );

  const onCancelar = () => {
    dispatch(LimpiarLineamiento({}));
    history.goBack();
  };

  const onAgregarInsitucion = useCallback(() => {
    history.push(ROUTES.INSTITUCIONES_AGREGAR);
  }, [history]);

  const onGuardar = useCallback(
    (event) => {
      event.preventDefault();
      dispatch(
        ModificarLineamiento(Resource.Query({ [FIX_URL]: id, ...formulario }))
      );
      history.goBack();
    },
    [dispatch, history, id, formulario]
  );
  return (
    <FormularioLineamiento
      {...{
        title: 'Editar Lineamiento',
        tipo_lineamiento,
        formulario,
        onChangeTipoLineamiento,
        onChangeInstitucion,
        onCancelar,
        onAgregarInsitucion,
        onGuardar,
        instituciones,
        onChangeField
      }}
    />
  );
};

import React from 'react';
import { SignUpLink } from '../../components/SignUp';
import { LoginLink } from '../../components/LogIn';

export default () => {
  return (
    <div>
      <h1>This is the landing page</h1>
      <SignUpLink />
      <LoginLink />
    </div>
  );
};

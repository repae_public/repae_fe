import React, { useCallback } from 'react';
import { Box, Card, IconButton } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { EditarIndicador } from '../../../../redux/actions';
import { useDispatch, useSelector } from 'react-redux';

export default (props) => {
  const { ods, indice } = props;
  const dispatch = useDispatch();
  const formulario = useSelector((state) => state.indicadores.formulario);
  const onEliminarOds = useCallback(() => {
    dispatch(
      EditarIndicador(
        Object.assign({}, formulario, {
          ods: formulario.ods.filter((value, index) => index !== indice)
        })
      )
    );
  });
  return (
    <Card className="card-box mb-4">
      <div className="card-header-alt d-flex justify-content-between p-4">
        <h6 className="font-weight-bold font-size-md mb-1 text-black">
          {ods.ODS_meta_nro}
        </h6>
        <span className="ml-2">{ods.ODS_meta_nombre}</span>
        <Box className="d-flex align-items-center">
          <IconButton
            className="ml-2"
            size="small"
            color="secondary"
            onClick={onEliminarOds}>
            <FontAwesomeIcon icon={['fas', 'trash']} />
          </IconButton>
        </Box>
      </div>
    </Card>
  );
};

import React, { useState } from 'react';
import { TablePagination } from '@material-ui/core';
import Fila from './Fila';

export default ({ value }) => {
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(0);
  const handlePageChange = (event, page) => {
    setPage(page);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(event.target.value);
  };
  return (
    <div className="table-responsive">
      <table className="table table-striped table-hover text-nowrap mb-0">
        <thead>
          <tr>
            <th className="text-left">Codigo</th>
            <th className="text-left">Nombre</th>
            <th className="text-left">Descripción</th>
            <th className="text-left">Creado por</th>
            <th className="text-center" colSpan="4">
              Acciones
            </th>
          </tr>
        </thead>

        <tbody>
          {value
            .slice(page * rowsPerPage, (page + 1) * rowsPerPage)
            .map((indicador) => (
              <Fila indicador={indicador} key={indicador.id} />
            ))}
        </tbody>
      </table>
      <TablePagination
        component="div"
        rowsPerPageOptions={[5, 10, 25]}
        count={value.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleRowsPerPageChange}
      />
    </div>
  );
};

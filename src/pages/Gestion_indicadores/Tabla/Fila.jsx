import React, { useCallback } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  FormControl,
  FormControlLabel,
  FormGroup,
  IconButton,
  Tooltip,
  Switch
} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { EliminarIndicador, ModificarIndicador } from '../../../redux/actions';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { FIX_URL } from 'ghost-stories/dist/constants';
import VerMas from './VerMas';
import ROUTES from '../../../constants/routes';
import { Link } from 'react-router-dom';

export default (props) => {
  const { indicador } = props;
  const dispatch = useDispatch();
  const onDeleteIndicador = useCallback(() => {
    dispatch(EliminarIndicador(Resource.Query({ [FIX_URL]: indicador.id })));
  }, [dispatch, indicador.id]);

  const onChangeVisibilidad = useCallback(
    (event) => {
      event.preventDefault();
      const updatedInstitucion = {
        ...indicador,
        publico: !indicador.publico
      };
      dispatch(
        ModificarIndicador(
          Resource.Query({ [FIX_URL]: indicador.id, ...updatedInstitucion })
        )
      );
    },
    [dispatch, indicador]
  );
  return (
    <tr key={indicador.id}>
      <td className="text-left">{indicador.codigo}</td>
      <td className="text-left">{indicador.nombre}</td>
      <td className="text-left">{indicador.descripcion}</td>
      <td className="text-left">{indicador.creadorNombre}</td>
      <td>
        <FormControl component="fieldset">
          <FormGroup aria-label="position" row>
            <FormControlLabel
              value="publico"
              control={
                <Switch
                  size="small"
                  color="secondary"
                  onChange={onChangeVisibilidad}
                  checked={indicador.publico}
                />
              }
              label="Público"
              labelPlacement="end"
            />
          </FormGroup>
        </FormControl>
      </td>
      <td>
        <Tooltip arrow title="Editar">
          <IconButton
            size="small"
            variant="outlined"
            color="secondary"
            component={Link}
            to={`${ROUTES.GESTION_INDICADORES_EDITAR.replace(
              ':id',
              indicador.id
            )}`}>
            <FontAwesomeIcon icon={['fa', 'edit']} />
          </IconButton>
        </Tooltip>
      </td>

      <td className="text-center">
        <Tooltip arrow title="Eliminar">
          <IconButton
            size="small"
            variant="outlined"
            color="secondary"
            onClick={onDeleteIndicador}>
            <FontAwesomeIcon icon={['fa', 'trash']} />
          </IconButton>
        </Tooltip>
      </td>

      <td className="text-center">
        <VerMas indicador={indicador} />
      </td>
    </tr>
  );
};

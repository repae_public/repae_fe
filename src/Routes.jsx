// lib imports
import React from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/styles';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { useSelector } from 'react-redux';

// styles

import MuiTheme from './theme';

import ROUTES from './constants/routes';

//pages - components
// import Login from './pages/Login';
import SignUp from './pages/SignUp';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import Indicadores from './pages/Indicadores';
import Perfil from './pages/Perfil';

import Delineamientos from './pages/Delineamientos';
import Delineamientos_agregar from './pages/Delineamientos/Agregar';
import Evaluaciones from './pages/Evaluaciones';
import Evaluaciones_agregar from './pages/Evaluaciones/Agregar';
import Evaluaciones_editar from './pages/Evaluaciones/Editar';
import Gestion_evaluaciones from './pages/Gestion_evaluaciones';
import Gestion_evaluaciones_agregar from './pages/Gestion_evaluaciones/Agregar';
import Gestion_indicadores from './pages/Gestion_indicadores';
import Gestion_indicadores_agregar from './pages/Gestion_indicadores/Agregar';
import Gestion_instituciones from './pages/Gestion_instituciones';
import Gestion_instituciones_agregar from './pages/Gestion_instituciones/Agregar';
import Gestion_lineamientos from './pages/Gestion_lineamientos';
import Gestion_lineamientos_agregar from './pages/Gestion_lineamientos/Agregar';
import Indicadores_agregar from './pages/Indicadores/Agregar';
import Indicadores_editar from './pages/Indicadores/Editar';
import Instituciones from './pages/Instituciones';
import Instituciones_agregar from './pages/Instituciones/Agregar';
import Usuarios from './pages/Usuarios';
import Usuarios_agregar from './pages/Usuarios/Agregar';
import Usuario_editar from './pages/Usuarios/Editar';
import Instituciones_editar from './pages/Instituciones/Editar';
import Lineamiento_editar from './pages/Delineamientos/Editar';
import Gestion_lineamientos_editar from './pages/Gestion_lineamientos/Editar';
import Gestion_instituciones_editar from './pages/Gestion_instituciones/Editar';
import Gestion_indicadores_editar from './pages/Gestion_indicadores/Editar';

// helpers
import MinimalLayout from './layouts/Minimal';
import LeftSidebar from './layouts/LeftSidebar';

const ROUTES_COMPONENTS = [
  [ROUTES.DASHBOARD, Dashboard],
  [ROUTES.GESTION_INDICADORES, Gestion_indicadores],
  [ROUTES.DELINEAMIENTOS_AGREGAR, Delineamientos_agregar],
  [ROUTES.EVALUACIONES_AGREGAR, Evaluaciones_agregar],
  [ROUTES.EVALUACIONES_EDITAR, Evaluaciones_editar],
  [ROUTES.GESTION_INDICADORES_AGREGAR, Gestion_indicadores_agregar],
  [ROUTES.INDICADORES_AGREGAR, Indicadores_agregar],
  [ROUTES.INDICADORES_EDITAR, Indicadores_editar],
  [ROUTES.INSTITUCIONES_AGREGAR, Instituciones_agregar],
  [ROUTES.USUARIOS_AGREGAR, Usuarios_agregar],
  [ROUTES.GESTION_INSTITUCIONES, Gestion_instituciones],
  [ROUTES.GESTION_INSTITUCIONES_AGREGAR, Gestion_instituciones_agregar],
  [ROUTES.GESTION_EVALUACIONES, Gestion_evaluaciones],
  [ROUTES.GESTION_EVALUACIONES_AGREGAR, Gestion_evaluaciones_agregar],
  [ROUTES.INDICADORES, Indicadores],
  [ROUTES.EVALUACIONES, Evaluaciones],
  [ROUTES.DELINEAMIENTOS, Delineamientos],
  [ROUTES.INSTITUCIONES, Instituciones],
  [ROUTES.USUARIOS, Usuarios],
  [ROUTES.PERFIL, Perfil],
  [ROUTES.GESTION_LINEAMIENTOS, Gestion_lineamientos],
  [ROUTES.GESTION_LINEAMIENTOS_AGREGAR, Gestion_lineamientos_agregar],
  [ROUTES.USUARIO_EDITAR, Usuario_editar],
  [ROUTES.INSTITUCIONES_EDITAR, Instituciones_editar],
  [ROUTES.LINEAMIENTOS_EDITAR, Lineamiento_editar],
  [ROUTES.GESTION_LINEAMIENTOS_EDITAR, Gestion_lineamientos_editar],
  [ROUTES.GESTION_INSTITUCIONES_EDITAR, Gestion_instituciones_editar],
  [ROUTES.GESTION_INDICADORES_EDITAR, Gestion_indicadores_editar]
];

const PRIVATE_ROUTES = ROUTES_COMPONENTS.map(([route, _]) => route);

// I want dashboard to only be accesible fo
export const Routes = () => {
  const location = useLocation();
  const user = useSelector((state) => state.authentication.user);
  const loggedIn = Resource.isData(user);
  return (
    <ThemeProvider theme={MuiTheme}>
      <Switch>
        <Redirect exact from={ROUTES.ROOT} to={ROUTES.LOG_IN} />

        <Route path={[ROUTES.LOG_IN, ROUTES.SIGN_UP, ROUTES.E404]}>
          {loggedIn ? (
            <Redirect to={ROUTES.DASHBOARD} />
          ) : (
            <MinimalLayout>
              <Switch location={location} key={location.pathname}>
                <Route path={ROUTES.LOG_IN} component={Login} />
                <Route path={ROUTES.SIGN_UP} component={SignUp} />
                <Route
                  path={ROUTES.E404}
                  component={() => <div>'This is the error page'</div>}
                />
              </Switch>
            </MinimalLayout>
          )}
        </Route>
        <Route exact path={PRIVATE_ROUTES}>
          {!loggedIn ? (
            <Redirect to={ROUTES.LOG_IN} />
          ) : (
            <LeftSidebar>
              <Switch location={location} key={location.pathname}>
                {ROUTES_COMPONENTS.map(([route, Component], index) => (
                  <Route
                    key={index.toString()}
                    exact
                    path={route}
                    component={Component}
                  />
                ))}
              </Switch>
            </LeftSidebar>
          )}
          ;
        </Route>
      </Switch>
    </ThemeProvider>
  );
};

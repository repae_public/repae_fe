import React, { Fragment } from 'react';
import clsx from 'clsx';
import { Avatar, Box, Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import ROUTES from '../../../../constants/routes';

const SidebarUserbox = (props) => {
  const { sidebarToggle, sidebarHover } = props;
  const user = useSelector((state) => state.authentication.user);
  const avatar = user
    .map((x) => x.additionalUserInfo.profile.picture)
    .getDataOr('');
  const userName = user
    .map((x) => x.additionalUserInfo.profile.name)
    .getDataOr('');

  return (
    <Fragment>
      <Box
        className={clsx('app-sidebar-userbox', {
          'app-sidebar-userbox--collapsed': sidebarToggle && !sidebarHover
        })}>
        <Avatar
          alt="Remy Sharp"
          src={avatar}
          className="app-sidebar-userbox-avatar"
        />
        <Box className="app-sidebar-userbox-name">
          <Box>
            <b>{userName}</b>
          </Box>
          <Box className="app-sidebar-userbox-btn-profile">
            <Button
              size="small"
              color="secondary"
              variant="contained"
              className="text-white"
              component={Link}
              to={ROUTES.PERFIL}>
              Ver perfil
            </Button>
          </Box>
        </Box>
      </Box>
    </Fragment>
  );
};

const mapStateToProps = (state) => ({
  sidebarToggle: state.ThemeOptions.sidebarToggle,
  sidebarHover: state.ThemeOptions.sidebarHover
});

export default connect(mapStateToProps)(SidebarUserbox);

import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Resource } from 'ghost-stories/dist/unions/Resource';

export const PrivateRoute = (props) => {
  const { children, ...rest } = props;

  const user = useSelector((state) => state.authentication.user);

  const render = (location) => {
    if (!Resource.isData(user))
      return (
        <Redirect
          to={{
            pathname: '/login',
            state: { from: location }
          }}
        />
      );

    return children;
  };

  return <Route {...rest} render={render} />;
};

export const ROLES_ADMINISTRADOR = 'Administrador';
export const ROLES_EVALUADOR = 'Evaluador';
export const ESTADOS_EVALUACIONES = [ROLES_ADMINISTRADOR, ROLES_EVALUADOR];

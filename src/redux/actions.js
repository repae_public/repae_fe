import {
  LOGIN,
  LOGIN_FACEBOOK,
  LOGIN_GOOGLE,
  LOGIN_TWITTER,
  INSTITUCIONES,
  ODS,
  EVALUACIONES,
  INDICADORES,
  DELINEAMIENTOS,
  GESTION_INDICADORES,
  GESTION_INSTITUCIONES,
  GESTION_EVALUACIONES,
  GESTION_USUARIOS,
  SEXO,
  RANGO_ETARIO,
  REGION,
  TIPO_INSTITUCION,
  TIPO_LINEAMIENTO,
  AGREGAR_EVALUACION,
  EDITAR_EVALUACION,
  LIMPIAR_EVALUACION,
  ELIMINAR_EVALUACION,
  MOSTRAR_MENSAJE,
  AGREGAR_INSTITUCION,
  LIMPIAR_INSTITUCION,
  EDITAR_INSTITUCION,
  ELIMINAR_INSTITUCION,
  AGREGAR_INDICADOR,
  LIMPIAR_INDICADOR,
  EDITAR_INDICADOR,
  ELIMINAR_INDICADOR,
  AGREGAR_LINEAMIENTO,
  LIMPIAR_LINEAMIENTO,
  EDITAR_LINEAMIENTO,
  ELIMINAR_LINEAMIENTO,
  MODIFICAR_EVALUACION,
  MODIFICAR_INDICADOR,
  ELIMINAR_USUARIO,
  EDITAR_USUARIO,
  LIMPIAR_USUARIO,
  MODIFICAR_USUARIO,
  AGREGAR_USUARIO,
  MODIFICAR_INSTITUCION,
  MODIFICAR_LINEAMIENTO
} from './actionTypes';

const create = (type) => (payload) => ({ type, payload });

export const Login = create(LOGIN);
export const LoginWithGoogle = create(LOGIN_GOOGLE);
export const LoginWithTwitter = create(LOGIN_TWITTER);
export const LoginWithFacebook = create(LOGIN_FACEBOOK);

//INSTITUCIONES
export const Instituciones = create(INSTITUCIONES);
export const AgregarInstitucion = create(AGREGAR_INSTITUCION);
export const LimpiarInstitucion = create(LIMPIAR_INSTITUCION);
export const EditarInstitucion = create(EDITAR_INSTITUCION);
export const EliminarInstitucion = create(ELIMINAR_INSTITUCION);
export const ModificarInstitucion = create(MODIFICAR_INSTITUCION);
//GESTION
export const GestionInstituciones = create(GESTION_INSTITUCIONES);

//EVALUACIONES
export const Evaluaciones = create(EVALUACIONES);
export const AgregarEvaluacion = create(AGREGAR_EVALUACION);
export const LimpiarEvaluacion = create(LIMPIAR_EVALUACION);
export const EditarEvaluacion = create(EDITAR_EVALUACION);
export const ModificarEvaluacion = create(MODIFICAR_EVALUACION);
export const EliminarEvaluacion = create(ELIMINAR_EVALUACION);
//GESTION
export const GestionEvaluaciones = create(GESTION_EVALUACIONES);

//INDICADORES
export const Indicadores = create(INDICADORES);
export const AgregarIndicador = create(AGREGAR_INDICADOR);
export const LimpiarIndicador = create(LIMPIAR_INDICADOR);
export const EditarIndicador = create(EDITAR_INDICADOR);
export const EliminarIndicador = create(ELIMINAR_INDICADOR);
export const ModificarIndicador = create(MODIFICAR_INDICADOR);
//GESTION
export const GestionIndicadores = create(GESTION_INDICADORES);

//LINEAMIENTOS
export const Delineamientos = create(DELINEAMIENTOS);
export const AgregarLineamiento = create(AGREGAR_LINEAMIENTO);
export const LimpiarLineamiento = create(LIMPIAR_LINEAMIENTO);
export const EditarLineamiento = create(EDITAR_LINEAMIENTO);
export const EliminarLineamiento = create(ELIMINAR_LINEAMIENTO);
export const ModificarLineamiento = create(MODIFICAR_LINEAMIENTO);

//USUARIOS
export const EliminarUsuario = create(ELIMINAR_USUARIO);
export const EditarUsuario = create(EDITAR_USUARIO);
export const LimpiarUsuario = create(LIMPIAR_USUARIO);
export const ModificarUsuario = create(MODIFICAR_USUARIO);
export const AgregarUsuario = create(AGREGAR_USUARIO);
//GESTION
export const GestionUsuarios = create(GESTION_USUARIOS);

//PARAMETROS/MENSAJES
export const Sexo = create(SEXO);
export const Region = create(REGION);
export const RangoEtario = create(RANGO_ETARIO);
export const TipoInstitucion = create(TIPO_INSTITUCION);
export const TipoLineamiento = create(TIPO_LINEAMIENTO);
export const MostrarMensaje = create(MOSTRAR_MENSAJE);

//ODS
export const Ods = create(ODS);

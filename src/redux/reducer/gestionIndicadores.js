import { merge } from 'remeda';
import { GESTION_INDICADORES } from '../actionTypes';
import { Resource } from 'ghost-stories/dist/unions/Resource';


const initial = {
    gestionIndicadores: Resource.Empty({}),
  };

export default (state = initial , action) => {
  switch (action.type) {
    case GESTION_INDICADORES:
      return merge(state.gestionIndicadores, { gestionIndicadores: action.payload });
    default:
      return state;
  }
};
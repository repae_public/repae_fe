import { merge } from 'remeda';
import {
  AGREGAR_USUARIO,
  EDITAR_USUARIO,
  GESTION_USUARIOS,
  LIMPIAR_USUARIO
} from '../actionTypes';
import { Resource } from 'ghost-stories/dist/unions/Resource';

const initial = {
  lista: Resource.Empty({}),
  agregar: Resource.Empty({}),
  formulario: {
    nombre: '',
    correo: '',
    rol: '',
    apellido: '',
    institucion: '',
    numero_telefono: ''
  }
};

export default (state = initial, action) => {
  switch (action.type) {
    case GESTION_USUARIOS:
      return merge(state, { lista: action.payload });
    case AGREGAR_USUARIO:
      return merge(state, { agregar: action.payload });
    case EDITAR_USUARIO:
      return merge(state, { formulario: action.payload });
    case LIMPIAR_USUARIO:
      return merge(state, { formulario: initial.formulario });
    default:
      return state;
  }
};

import { BASE_URL } from '../../constants/vars';
import {
  qsGetter,
  jsonPoster,
  jsonDelete,
  jsonPut
} from 'ghost-stories/dist/fetch/helpers';

//GET
export const obtenerInstituciones = qsGetter(`${BASE_URL}/instituciones`);
export const obtenerODS = qsGetter(`${BASE_URL}/ods_meta`);
export const obtenerEvaluaciones = qsGetter(`${BASE_URL}/evaluaciones`);
export const obtenerIndicadores = qsGetter(`${BASE_URL}/indicadores`);
export const obtenerDelineamientos = qsGetter(`${BASE_URL}/delineamientos`);
export const obtenerUsuarios = qsGetter(`${BASE_URL}/perfiles`);
export const obtenerSexo = qsGetter(`${BASE_URL}/sexo`);
export const obtenerTipoInstitucion = qsGetter(`${BASE_URL}/tipo_institucion`);
export const obtenerTipoLineamiento = qsGetter(`${BASE_URL}/tipo_lineamiento`);
export const obtenerRegion = qsGetter(`${BASE_URL}/region`);
export const obtenerRangoEtario = qsGetter(`${BASE_URL}/rango_etario`);

//POST//PUT EVALUACION
export const postEvaluacion = jsonPoster(`${BASE_URL}/evaluaciones`);
export const putEvaluacion = jsonPut(`${BASE_URL}/evaluaciones`);
//POST//PUT INDICADOR
export const postIndicador = jsonPoster(`${BASE_URL}/indicadores`);
export const putIndicador = jsonPut(`${BASE_URL}/indicadores`);
//POST//PUT INSTITUCION
export const postInstitucion = jsonPoster(`${BASE_URL}/instituciones`);
export const putInstitucion = jsonPut(`${BASE_URL}/instituciones`);
//POST//PUT LINEAMIENTO
export const postLineamiento = jsonPoster(`${BASE_URL}/delineamientos`);
export const putLineamiento = jsonPut(`${BASE_URL}/delineamientos`);

//POST//PUT USUARIO
export const postUsuario = jsonPoster(`${BASE_URL}/perfiles`);
export const putUsuario = jsonPut(`${BASE_URL}/perfiles`);

//DELETE EVALUACION
export const deleteEvaluacion = jsonDelete(`${BASE_URL}/evaluaciones`);
//DELETE INSTITUCION
export const deleteInstitucion = jsonDelete(`${BASE_URL}/instituciones`);
//DELETE INDICADOR
export const deleteIndicador = jsonDelete(`${BASE_URL}/indicadores`);
//DELETE LINEAMIENTO
export const deleteLineamiento = jsonDelete(`${BASE_URL}/delineamientos`);
//DELETE USUARIO
export const deleteUsuario = jsonDelete(`${BASE_URL}/perfiles`);

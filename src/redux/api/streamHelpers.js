import { pipe } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { mergeDeepRight } from 'ramda';
import { firebase } from '../../services/Firebase';

export const addAuth = () =>
  pipe(
    mergeMap((params) =>
      firebase.getIdToken().then((value) =>
        mergeDeepRight(params, {
          options: {
            headers: {
              Authorization: `Bearer ${value}`
            }
          }
        })
      )
    )
  );

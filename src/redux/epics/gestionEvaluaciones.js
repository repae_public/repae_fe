import { GESTION_EVALUACIONES } from '../actionTypes';
import { obtenerEvaluaciones } from '../api';
import { GestionEvaluaciones } from '../actions';
import { toParams, makeRequest } from 'ghost-stories/dist/streams/resource';
import { map } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import { addAuth } from '../api/streamHelpers';

const solicitarGestionEvaluaciones = (action$) =>
  action$.pipe(
    ofType(GESTION_EVALUACIONES),
    toParams,
    addAuth(),
    makeRequest(obtenerEvaluaciones),
    map(GestionEvaluaciones)
  );

export default combineEpics(solicitarGestionEvaluaciones);

import { combineEpics, ofType } from 'redux-observable';
import { map, mergeMap } from 'rxjs/operators';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { Login } from '../actions';
import * as ActionTypes from '../actionTypes';
import { firebase } from '../../services/Firebase';

const loginWithGoogle = (action$, state$) =>
  action$.pipe(
    ofType(ActionTypes.LOGIN_GOOGLE),
    // acá tengo que crear el recurso user
    mergeMap(() => {
      const user = firebase.doGoogleRegister();
      return Resource.mapPromise({}, user);
    }),
    map(Login)
  );

const loginWithFacebook = (action$, state$) =>
  action$.pipe(
    ofType(ActionTypes.LOGIN_FACEBOOK),
    mergeMap(() => {
      const user = firebase.doFacebookRegister();
      return Resource.mapPromise({}, user);
    }),
    map(Login)
  );

const loginWithTwitter = (action$, state$) =>
  action$.pipe(
    ofType(ActionTypes.LOGIN_TWITTER),
    mergeMap(() => {
      const user = firebase.doTwitterRegister();
      return Resource.mapPromise({}, user);
    }),
    map(Login)
  );

export default combineEpics(
  loginWithGoogle,
  loginWithFacebook,
  loginWithTwitter
);

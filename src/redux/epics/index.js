import { combineEpics } from 'redux-observable';
import authentication from './authentication';
import instituciones from './instituciones';
import ods from './ods';
import evaluaciones from './evaluaciones';
import indicadores from './indicadores';
import delineamientos from './delineamientos';
import gestionIndicadores from './gestionIndicadores';
import gestionInstituciones from './gestionInstituciones';
import gestionEvaluaciones from './gestionEvaluaciones';
import gestionUsuarios from './gestionUsuarios';
import parametros from './parametros';

export const rootEpic = combineEpics(
  authentication,
  instituciones,
  ods,
  evaluaciones,
  indicadores,
  delineamientos,
  gestionIndicadores,
  gestionInstituciones,
  gestionEvaluaciones,
  gestionUsuarios,
  parametros
);

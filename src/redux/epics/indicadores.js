import { toParams, makeRequest } from 'ghost-stories/dist/streams/resource';
import { map, filter, mergeMap } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { addAuth } from '../api/streamHelpers';

import {
  AGREGAR_INDICADOR,
  MODIFICAR_INDICADOR,
  ELIMINAR_INDICADOR,
  INDICADORES
} from '../actionTypes';

import {
  obtenerIndicadores,
  postIndicador,
  deleteIndicador,
  putIndicador
} from '../api';

import {
  AgregarIndicador,
  ModificarIndicador,
  EliminarIndicador,
  Indicadores,
  MostrarMensaje
} from '../actions';

const solicitarIndicadores = (action$) =>
  action$.pipe(
    ofType(INDICADORES),
    toParams,
    addAuth(),
    makeRequest(obtenerIndicadores),
    map((res) => res.map(({ data }) => data)),
    map(Indicadores)
  );

const guardarIndicadores = (action$) =>
  action$.pipe(
    ofType(AGREGAR_INDICADOR),
    toParams,
    addAuth(),
    makeRequest(postIndicador),
    map(AgregarIndicador)
  );

const actualizarIndicador = (action$) =>
  action$.pipe(
    ofType(MODIFICAR_INDICADOR),
    toParams,
    addAuth(),
    makeRequest(putIndicador),
    map(ModificarIndicador)
  );

const onGuardar = (action$) =>
  action$.pipe(
    ofType(AGREGAR_INDICADOR, MODIFICAR_INDICADOR),
    filter((action) => Resource.isData(action.payload)),
    mergeMap(() => [
      MostrarMensaje({
        open: true,
        mensaje: 'Indicador guardado con éxito',
        tipo: 'success'
      }),
      Indicadores(Resource.Query({}))
    ])
  );

const borrarIndicador = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_INDICADOR),
    toParams,
    addAuth(),
    makeRequest(deleteIndicador),
    map(EliminarIndicador)
  );

const onEliminar = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_INDICADOR),
    filter((action) => Resource.isData(action.payload)),
    mergeMap(() => [
      Indicadores(Resource.Query({})),
      MostrarMensaje({
        open: true,
        mensaje: 'Eliminado con éxito',
        tipo: 'success'
      })
    ])
  );

const onEliminarError = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_INDICADOR),
    filter((action) => Resource.isError(action.payload)),
    map(() =>
      MostrarMensaje({
        open: true,
        mensaje: 'Error al eliminar',
        tipo: 'error'
      })
    )
  );

export default combineEpics(
  solicitarIndicadores,
  guardarIndicadores,
  onGuardar,
  borrarIndicador,
  actualizarIndicador,
  onEliminar,
  onEliminarError
);

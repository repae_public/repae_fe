import {
  AGREGAR_USUARIO,
  ELIMINAR_USUARIO,
  GESTION_USUARIOS,
  MODIFICAR_USUARIO
} from '../actionTypes';
import {
  deleteUsuario,
  obtenerUsuarios,
  postUsuario,
  putUsuario
} from '../api';
import {
  AgregarUsuario,
  EliminarUsuario,
  GestionUsuarios,
  ModificarUsuario,
  MostrarMensaje
} from '../actions';
import { toParams, makeRequest } from 'ghost-stories/dist/streams/resource';
import { map } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import { addAuth } from '../api/streamHelpers';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { filter, mergeMap } from 'rxjs/operators';

const solicitarGestionUsuarios = (action$) =>
  action$.pipe(
    ofType(GESTION_USUARIOS),
    toParams,
    addAuth(),
    makeRequest(obtenerUsuarios),
    map(GestionUsuarios)
  );

const guardarUsuario = (action$) =>
  action$.pipe(
    ofType(AGREGAR_USUARIO),
    toParams,
    addAuth(),
    makeRequest(postUsuario),
    map(AgregarUsuario)
  );

const actualizarUsuario = (action$) =>
  action$.pipe(
    ofType(MODIFICAR_USUARIO),
    toParams,
    addAuth(),
    makeRequest(putUsuario),
    map(ModificarUsuario)
  );

const onGuardar = (action$) =>
  action$.pipe(
    ofType(AGREGAR_USUARIO, MODIFICAR_USUARIO),
    filter((action) => Resource.isData(action.payload)),
    map(() =>
      MostrarMensaje({
        open: true,
        mensaje: 'Usuario guardado con éxito',
        tipo: 'success'
      })
    )
  );

const onGuardarError = (action$) =>
  action$.pipe(
    ofType(AGREGAR_USUARIO, MODIFICAR_USUARIO),
    filter((action) => Resource.isError(action.payload)),
    map(() =>
      MostrarMensaje({
        open: true,
        mensaje: 'Error al guardar el usuario',
        tipo: 'error'
      })
    )
  );

const borrarUsuario = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_USUARIO),
    toParams,
    addAuth(),
    makeRequest(deleteUsuario),
    map(EliminarUsuario)
  );

const onEliminar = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_USUARIO),
    filter((action) => Resource.isData(action.payload)),
    mergeMap(() => [
      GestionUsuarios(Resource.Query({})),
      MostrarMensaje({
        open: true,
        mensaje: 'Eliminado con éxito',
        tipo: 'success'
      })
    ])
  );

const onEliminarError = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_USUARIO),
    filter((action) => Resource.isError(action.payload)),
    map(() =>
      MostrarMensaje({
        open: true,
        mensaje: 'Error al eliminar',
        tipo: 'error'
      })
    )
  );

export default combineEpics(
  solicitarGestionUsuarios,
  borrarUsuario,
  onEliminar,
  onEliminarError,
  guardarUsuario,
  actualizarUsuario,
  onGuardar,
  onGuardarError
);

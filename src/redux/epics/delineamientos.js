import {
  AGREGAR_LINEAMIENTO,
  DELINEAMIENTOS,
  ELIMINAR_LINEAMIENTO,
  MODIFICAR_LINEAMIENTO
} from '../actionTypes';
import {
  deleteLineamiento,
  obtenerDelineamientos,
  postLineamiento,
  putLineamiento
} from '../api';
import {
  AgregarLineamiento,
  Delineamientos,
  EliminarLineamiento,
  MostrarMensaje,
  LimpiarLineamiento,
  ModificarLineamiento
} from '../actions';
import { toParams, makeRequest } from 'ghost-stories/dist/streams/resource';
import { map, filter, mergeMap } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { addAuth } from '../api/streamHelpers';

const solicitarDelineamientos = (action$) =>
  action$.pipe(
    ofType(DELINEAMIENTOS),
    toParams,
    addAuth(),
    makeRequest(obtenerDelineamientos),
    map(Delineamientos)
  );

const guardarLineamiento = (action$) =>
  action$.pipe(
    ofType(AGREGAR_LINEAMIENTO),
    toParams,
    addAuth(),
    makeRequest(postLineamiento),
    map(AgregarLineamiento)
  );

const actualizarLineamiento = (action$) =>
  action$.pipe(
    ofType(MODIFICAR_LINEAMIENTO),
    toParams,
    addAuth(),
    makeRequest(putLineamiento),
    map(ModificarLineamiento)
  );

const onGuardar = (action$) =>
  action$.pipe(
    ofType(AGREGAR_LINEAMIENTO, MODIFICAR_LINEAMIENTO),
    filter((action) => Resource.isData(action.payload)),
    mergeMap(() => [
      MostrarMensaje({
        open: true,
        mensaje: 'Lineamiento guardado con éxito',
        tipo: 'success'
      }),
      Delineamientos(Resource.Query({})),
      LimpiarLineamiento()
    ])
  );

const onGuardarError = (action$) =>
  action$.pipe(
    ofType(AgregarLineamiento),
    filter((action) => Resource.isError(action.payload)),
    map(() =>
      MostrarMensaje({
        open: true,
        mensaje: 'Error al guardar el lineamiento',
        tipo: 'error'
      })
    )
  );

const onGuardarEditado = (action$) =>
  action$.pipe(
    ofType(MODIFICAR_LINEAMIENTO),
    filter((action) => Resource.isData(action.payload)),
    mergeMap(() => [
      Delineamientos(Resource.Query({})),
      MostrarMensaje({
        open: true,
        mensaje: 'Lineamiento editado con éxito',
        tipo: 'success'
      }),
      LimpiarLineamiento()
    ])
  );

const onGuardarEditadoError = (action$) =>
  action$.pipe(
    ofType(MODIFICAR_LINEAMIENTO),
    filter((action) => Resource.isError(action.payload)),
    mergeMap(() => [
      Delineamientos(Resource.Query({})),
      MostrarMensaje({
        open: true,
        mensaje: 'No se pudo editar el lineamiento',
        tipo: 'error'
      }),
      LimpiarLineamiento()
    ])
  );

const borrarLineamiento = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_LINEAMIENTO),
    toParams,
    addAuth(),
    makeRequest(deleteLineamiento),
    map(EliminarLineamiento)
  );

const onEliminar = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_LINEAMIENTO),
    filter((action) => Resource.isData(action.payload)),
    mergeMap(() => [
      Delineamientos(Resource.Query({})),
      MostrarMensaje({
        open: true,
        mensaje: 'Lineamiento eliminado con éxito',
        tipo: 'success'
      })
    ])
  );

const onEliminarError = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_LINEAMIENTO),
    filter((action) => Resource.isError(action.payload)),
    map(() =>
      MostrarMensaje({
        open: true,
        mensaje: 'Error al eliminar el lineamiento',
        tipo: 'error'
      })
    )
  );

export default combineEpics(
  solicitarDelineamientos,
  guardarLineamiento,
  onGuardar,
  borrarLineamiento,
  onEliminar,
  onEliminarError,
  actualizarLineamiento,
  onGuardarError,
  onGuardarEditado,
  onGuardarEditadoError
);

export const mapInstituciones = (data) => {
  return {
    id: data.id,
    meta: data.meta,
    descripcion: data.descripcion,
    codigo: data.codigo,
    nombre: data.nombre
  };
};
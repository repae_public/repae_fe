export const mapGestionInstituciones = (data) => {
  return {
    id: data.id,
    meta: data.meta,
    descripcion: data.descripcion,
    codigo: data.codigo,
    nombre: data.nombre,
    tipo: data.tipo
  };
};

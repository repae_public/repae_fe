export const mapDelineamientos = (data) => {
    return {
      id: data.id,
      codigo: data.codigo,
      descripcion: data.descripcion,
      nombre: data.nombre
    };
  };
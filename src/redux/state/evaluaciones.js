export const mapEvaluaciones = (data) => {
  return {
    id: data.id,
    estado: data.estado,
    titulo: data.titulo,
    proyecto: data.proyecto
  };
};

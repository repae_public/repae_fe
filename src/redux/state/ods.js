export const mapODS = (data) => {
  return {
    id: data.id,
    ods_meta_nro: data.ods_meta_nro,
    ods_obj_id: data.ODS_obj_id,
    ods_meta_nombre: data.ODS_meta_nombre
  };
};
